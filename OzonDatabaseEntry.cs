﻿using System;

namespace EShop
{
    /// <summary>
    /// Класс доступа к базе данных.
    /// Гарантирует создание только одной точки доступа к БД
    /// </summary>
    public class OzonDatabaseEntry
    {
        private static readonly OzonDatabaseContext instance = new OzonDatabaseContext();
        
        /// <summary>
        /// Точка доступа к контексту базе данных
        /// </summary>
        public static OzonDatabaseContext Instance
        {
            get { return instance; }
        }

        protected OzonDatabaseEntry() { }
    }
}