﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using EShop.Models;
using EShop.Entities;

namespace EShop.Controllers
{
    public class ValuesController : ApiController
    {
        OzonDatabaseContext context = OzonDatabaseEntry.Instance;

        // GET api/values
        public IEnumerable<Offer> Get()
        {
            // Delete data about offer for minimize size of JSON-response
            List<Offer> offers = context.Offers.Include("Categories").Include("Currency").Include("Pictures").ToList();
            foreach (var offer in offers)
            {
                foreach (var category in offer.Categories)
                {
                    category.Offers = null;
                }
            }

            return offers;
        }

        [Authorize]
        // GET api/values/5
        public Offer Get(int id)
        {
            return context.Offers.Find(new object[] { id });
        }

        // POST api/values
        [HttpPost]
        public List<Offer> Post([FromBody] SearchQuery searchQuery)
        {
            if (searchQuery == null || searchQuery.searchQuery == null)
                return null;

            List<Offer> offers = context.Offers.Include("Pictures").Include("Currency").ToList();

            StringComparison comparsionType = (searchQuery.registerCaseImportant) ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
            return offers.FindAll(x => (x.name.IndexOf(searchQuery.searchQuery, comparsionType) > -1));
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}