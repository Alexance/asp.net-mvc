﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EShop.Entities.OfferElements
{
    public class Vendor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int vendorId { get; set; }
        public string vendor { get; set; }
        public string vendorCode { get; set; }

        [Required]
        public Offer offer { get; set; }
    }
}
