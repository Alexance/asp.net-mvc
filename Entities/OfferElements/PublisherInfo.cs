﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EShop.Entities.OfferElements
{
    public class PublisherInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int publisherId { get; set; }

        [MaxLength(50)]
        public string author { get; set; }
        [MaxLength(50)]
        public string publisher { get; set; }
        public int year { get; set; }
        [MaxLength(30)]
        public string ISBN { get; set; }
        public int page_extent { get; set; }
        [MaxLength(30)]
        public string series { get; set; }

        [Required]
        public Offer offer { get; set; }
    }
}
