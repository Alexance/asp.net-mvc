﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace EShop.Entities
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int categoryId { get; set; }
        public Category ParentCategory { get; set; }
        public string value { get; set; }

        public virtual List<Offer> Offers { get; set; }
    }
}