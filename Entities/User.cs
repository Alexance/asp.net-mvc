﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EShop.Entities
{
    public class User
    {
        public int UserId { get; set; }

        [Required]
        [MaxLength(16)]
        public string Login { get; set; }
        [Required]
        [MaxLength(64)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(64)]
        public string SecondName { get; set; }
        [Required]
        [MaxLength(16)]
        public string Password { get; set; }

        [Required]
        [MaxLength(256)]
        public string Address { get; set; }

        public virtual List<UserOffers> Offers { get; set; }
    }
}
